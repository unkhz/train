
# Departures board

This app consumes [Huxley](https://huxley.apphb.com/) to make a departures board for train stations across the UK.

#### Pre-Requisites

- Ensure you're running Node 6
- Run `npm install` inside the directory

#### Instructions

- Run `npm run start` to serve the application
- Run `npm run dev` to serve the application and watch scripts for changes
- Run `npm run unit` to run unit tests defined by `*.spec.js`
- Run `npm run unit-watch` to run unit tests and watch for changes
- Run `npm run test` to run functional tests in the `tests/` folder

Functional tests are written with TestCafé, you can [read more about the API](https://devexpress.github.io/testcafe/documentation/test-api/).
