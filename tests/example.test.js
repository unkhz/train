
const expect = require('chai').expect;
const Selector = require('testcafe').Selector;
const getElement = Selector(sel => document.querySelector(sel));
const getElements = Selector(sel => document.querySelectorAll(sel));
const componentSelector = (componentId) => `[data-component-id='${componentId}']`;

fixture('index').page('http://127.0.0.1:5000');

test('should contain a heading containing the task title', () => {
  return getElement('h1').then(element => {
    expect(element.textContent).to.equal('Train Departures');
  });
});

test('should contain one StationSearch input with a placeholder', (t) => {
  return getElement(componentSelector('stationSearch')).then(element => {
    expect(element.tagName).to.equal('input');
    expect(element.attributes.placeholder).to.equal('Station name or code');
  });
});

test('should update DepartureTable when 3 characters are input to StationSearch', (t) => {
  const search = componentSelector('stationSearch');
  const table = componentSelector('departureTable');
  return getElement(componentSelector('stationSearch'))
    .then(() => t.typeText(search, 'yor'))
    .then(() => getElement(search))
    .then(element => expect(element.value).to.equal('yor'))
    // wait for content in table
    .then(() => getElement(table + ' tr td'))
    .then(() => getElement(table))
    .then(elements => expect(elements.childElementCount).to.be.above(1))
  ;
});

test('should update RecentList after searching', (t) => {
  const search = componentSelector('stationSearch');
  const list = componentSelector('recentList');
  return getElement(componentSelector('stationSearch'))
    .then(() => t.typeText(search, 'leeds'))
    // wait for content in list
    .then(() => getElement(list + ' a'))
    .then(() => getElement(list + ' ul'))
    .then(element => expect(element.childElementCount).to.equal(1))
  ;
});

test('should update search after clicking RecentList', (t) => {
  const search = componentSelector('stationSearch');
  const list = componentSelector('recentList');
  return getElement(componentSelector('stationSearch'))
    // type partial match
    .then(() => t.typeText(search, 'leed'))
    // wait for content in list
    .then(() => getElement(list + ' a'))
    .then(() => t.click(list + ' a'))
    .then(() => getElement(search))
    // expect full name
    .then(element => expect(element.value).to.equal('Leeds'))
  ;
});

