const webpack = require('webpack');
const path = require('path');
const dir = {
  src: path.resolve(__dirname, "client"),
  output: path.resolve(__dirname, "assets"),
};

module.exports = {
  entry: [
    'babel-polyfill',
    path.join(dir.src, 'main.js'),
  ],

  output: {
    path: dir.output,
    filename: 'script.js'
  },

  module: {
    rules: [
      {
        loader: "babel-loader",
        include: [
          dir.src,
        ],
        test: /\.js$/,
        query: {
          plugins: ['transform-runtime'],
          presets: ['es2015', 'stage-1'],
        }
      },
    ],
  }
}