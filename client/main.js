import 'whatwg-fetch';
import {compareTwoStrings} from 'string-similarity';

import {setState, onState, onAction, callAction} from './modules/state';
import createRequestForHuxley from './modules/huxley';
import App from './components/App';


// TODO move to config
const huxleyEndPoint = 'https://huxley.apphb.com/';
const huxleyApiKey = '074a45ee-2d61-43a9-ab7d-42439d112733';

const getStations = createRequestForHuxley(fetch, huxleyEndPoint, huxleyApiKey, 'crs');
const getDepartures = createRequestForHuxley(fetch, huxleyEndPoint, huxleyApiKey, 'departures');

// Context
const context = {
  document,
  setState: (key, value) => context.state = setState(context.state, key, value),
  onState: (key, rendrer) => context.state = onState(context.state, key, rendrer),
  callAction: (key, value) => context.state = callAction(context.state, key, value),
  onAction: (key, reaction) => context.state = onAction(context.state, key, reaction),
  state: {},
};

// TODO add to debug config
window.context = context;


// Views
const appEl = document.getElementById('app');
appEl.appendChild(App(context));


// Reactions
context.onAction('search', (value) => {
  context.setState('searchContent', value);
  if (value.length < 3) {
    return;
  }
  console.log('[Main] Finding best matching station for', value);
  getStations(value).then((response) => response.json())
    .then((stations) => stations.length ? findBestStationForSearch(value, stations) : undefined)
    .then((station) => station ? getDepartures(station.crsCode).then((response) => response.json()) : undefined)
    .then((departures) => context.callAction('fetchedDepartures', departures));
});

context.onAction('selectRecent', (value) => {
  console.log('[Main] Selected recent', value);
  context.callAction('search', value);
});

let debounceUpdateRecentListTimeoutId;
context.onAction('fetchedDepartures', (departures) => {
  context.setState('departures', departures);

  // Update recent list after a while, so that partial searches are not caught
  if (departures) {
    clearInterval(debounceUpdateRecentListTimeoutId);
    debounceUpdateRecentListTimeoutId = setTimeout(() => (
      context.callAction('updateRecentList', departures.locationName)
    ), 2000);
  }
});

context.onAction('updateRecentList', (locationName) => {
  if (localStorage) {
    const list = JSON.parse(localStorage.getItem('recentList') || '[]');
    let newList = [...list];
    if (locationName) {
      newList = [
        locationName,
        ...list
          .filter((item) => item !== locationName)
          .slice(0,5),
      ];
      localStorage.setItem('recentList', JSON.stringify(newList));
    }
    context.setState('recentList', newList);
  } else {
    context.setState('recentList', []);
  }
});


context.callAction('updateRecentList', undefined);


// Utils
function findBestStationForSearch(search, stations) {
  if (stations.length === 0) {
    return;
  }
  return stations.sort((station) => compareTwoStrings(search, station.stationName))[0];
}