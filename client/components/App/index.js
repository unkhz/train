import {createElement, componentSelector} from '../../modules/dom';

import StationSearch from '../StationSearch';
import DepartureTable from '../DepartureTable';
import RecentList from '../RecentList';

export default function App(context, props={}, attributes={}) {
  const {document, onState, addReaction} = context;

  let appEl = render(context, props, attributes);

  // Render when departures change
  onState('departures', ({departures={}}) => {
    appEl.replaceChild(renderTitle(context, departures), appEl.querySelector('#departureHeading'));
    appEl.replaceChild(renderDepartureTable(context, departures), appEl.querySelector(componentSelector('departureTable')));
  });

  onState('recentList', (state) => {
    appEl.replaceChild(renderRecentList(context, state.recentList), appEl.querySelector(componentSelector('recentList')));
  });

  onState('searchContent', (state) => {
    // NB! This kind of hacking is why we need virtual DOM. Cannot re-render, as we'd lose focus.
    appEl.querySelector(componentSelector('stationSearch')).value = state.searchContent || '';
  });

  return appEl;
}

function render(context, props={}, attributes={}) {
  const {document, state} = context;

  return createElement(document, ['div', props, attributes, [
    renderStationSearch(context, state.searchContent),
    renderRecentList(context, state.recentList),
    renderTitle(context, state.departures),
    renderDepartureTable(context, state.departures),
  ]]);
}

function renderStationSearch(context, search) {
  return StationSearch(context, {value: search});
}

function renderRecentList(context, recentList) {
  return RecentList(context, {data: recentList});
}

function renderDepartureTable(context, departures={}) {
  return DepartureTable(context, {data: departures.trainServices});
}

function renderTitle(context, departures={}) {
  const {document} = context;
  const locationName = departures.locationName;
  const message = locationName ? 'Showing departures from ' + locationName : 'No results';
  return createElement(document, ['h2', {id: 'departureHeading'}, {}, [message]]);
}