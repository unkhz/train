import {assert} from 'chai';

import {document, componentSelector} from '../../testUtils/dom';

import App from './index';

describe('components/App', () => {

  let stateListeners = {};
  let context;
  beforeEach(() => {
    context = {
      document,
      onState: (key, cb) => stateListeners[key] = cb,
      state: {},
    }
  });

  it('should create a div' , () => {
    const el = App(context);
    assert.equal(el.tagName, 'DIV', 'should be div');
  });

  it('should have station search', () => {
    const el = App(context);
    assert.equal(el.querySelectorAll(componentSelector('stationSearch')).length, 1);
  });

  it('should have departure table', () => {
    const el = App(context);
    assert.equal(el.querySelectorAll(componentSelector('departureTable')).length, 1);
  });

  it('should update departure table on state change to valid data', () => {
    const el = App(context);
    assert.equal(el.querySelectorAll(componentSelector('departureTable') + ' tr').length, 1);
    stateListeners['departures']({
      departures: {
        locationName: 'York',
        trainServices: [
          {operator: 'Virgin', std: '11.10', etd: 'On time', destination: [{locationName: 'To 1'}]},
          {operator: 'Northern', std: '11.11', etd: 'On time', destination: [{locationName: 'To 2'}]},
          {operator: 'Space X', std: '11.12', etd: 'Trains delayed. Sad!', destination: [{locationName: 'To 3'}]},
        ]
      }
    });

    assert.equal(el.querySelectorAll(componentSelector('departureTable') + ' tr').length, 4);
  });

  it('should update departure table on state change to empty data', () => {
    const el = App(context);
    stateListeners['departures']({
      departures: {
        locationName: 'York',
        trainServices: [
          {operator: 'Virgin', std: '11.10', etd: 'On time', destination: [{locationName: 'To 1'}]},
          {operator: 'Northern', std: '11.11', etd: 'On time', destination: [{locationName: 'To 2'}]},
          {operator: 'Space X', std: '11.12', etd: 'Trains delayed. Sad!', destination: [{locationName: 'To 3'}]},
        ]
      }
    });
    stateListeners['departures']({});
    assert.equal(el.querySelectorAll(componentSelector('departureTable') + ' tr').length, 1);
  });

});