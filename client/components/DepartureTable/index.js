import {createElement} from '../../modules/dom';

const isOnTime = (item) => item.etd === 'On time';

// The departures board must contain: destination, train operator, departure time, platform, delays
const contentSelectors = {
  'Destination': (item) => item.destination[0].locationName,
  'Operator': (item) => item.operator,
  'Departs': (item) => item.std,
  'Delays': (item) => isOnTime(item) ? '' : item.etd,
};

const tableClassSelectors = {
  'is-empty': (data) => !data || !data.length,
}

const rowClassSelectors = {
  'is-delayed': (item) => !isOnTime(item),
}

export default function DepartureTable({document, setState, onState}, props={}, attributes={}) {

  const {data=[], ...restOfProps} = props;

  const tableProps = Object.assign({}, {
    className: getClassNameFromSelectorMap(tableClassSelectors, data)
  }, restOfProps);

  const tableAttributes = Object.assign({}, {
    'data-component-id': 'departureTable'
  }, attributes);


  let rows = [

    // Header
    ['tr', {}, {}, Object.keys(contentSelectors).map((title) => (
      ['th', {}, {}, [title]]
    ))],

    // Data rows
    ...data.map((item, i) => {
      const className = getClassNameFromSelectorMap(rowClassSelectors, item);

      return ['tr', { className }, {}, Object.keys(contentSelectors).map((title) => (
        ['td', {}, {}, [
          ['span', {className: 'departureTable-info'}, {}, [title + ': ']],
          ['span', {className: 'departureTable-content'}, {}, [contentSelectors[title](item)]]
        ]]
      ))]
    })

  ];

  return createElement(document, ['table', tableProps, tableAttributes, rows]);
}

function getClassNameFromSelectorMap(selectorMap, data) {
  return Object.keys(selectorMap).reduce((all, classNameToCheck) => (
    all.concat([selectorMap[classNameToCheck](data) ? classNameToCheck : ''])
  ), []).join(' ');
}