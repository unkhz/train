import {assert} from 'chai';

import {document} from '../../testUtils/dom';

import DepartureTable from './index';

describe('components/DepartureTable', () => {

  it('should create a table' , () => {
    const el = DepartureTable({document});
    assert.equal(el.tagName, 'TABLE', 'should be table');
  });

  it('should have header row without data', () => {
    const el = DepartureTable({document});
    assert.equal(el.querySelectorAll('tr').length, 1);
    assert.equal(el.querySelectorAll('th').length, 4);
  });

  it('should have data rows', () => {
    const el = DepartureTable({document}, {data:[
      {operator: 'Virgin', std: '11.10', etd: 'On time', destination: [{locationName: 'To 1'}]},
      {operator: 'Northern', std: '11.11', etd: 'On time', destination: [{locationName: 'To 2'}]},
      {operator: 'Space X', std: '11.12', etd: '11:25', destination: [{locationName: 'To 3'}]},
    ]});
    assert.deepEqual(Array.from(el.querySelectorAll('td')).map(el => el.textContent), [
      'Destination: To 1', 'Operator: Virgin',   'Departs: 11.10', 'Delays: ',
      'Destination: To 2', 'Operator: Northern', 'Departs: 11.11', 'Delays: ',
      'Destination: To 3', 'Operator: Space X',  'Departs: 11.12', 'Delays: 11:25',
    ]);
  });

  it('should have classes in rows', () => {
    const el = DepartureTable({document}, {data:[
      {operator: 'Virgin', std: '11.10', etd: 'On time', destination: [{locationName: 'To 1'}]},
      {operator: 'Northern', std: '11.11', etd: 'On time', destination: [{locationName: 'To 2'}]},
      {operator: 'Space X', std: '11.12', etd: 'Trains delayed. Sad!', destination: [{locationName: 'To 3'}]},
    ]});
    assert.equal(el.querySelectorAll('tr.is-delayed').length, 1);
  });

});