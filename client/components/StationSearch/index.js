import {createElement} from '../../modules/dom';

let searchDebounceTimeoutId;
function debounce(delay) {
  clearTimeout(searchDebounceTimeoutId);
  return new Promise((resolve) => {
    searchDebounceTimeoutId = setTimeout(resolve, delay);
  });
}

export default function StationSearch({document, callAction}, props={}, attributes={}) {
  const onInput = (evt) => {
    debounce(300).then(() => callAction('search', evt.target.value));
    evt.preventDefault();
  };

  const {value, ...restOfProps} = props;

  const inputProps = Object.assign({}, {
    placeholder: 'Station name or code',
    value: props.value || '',
    onInput,
    onChange: onInput,
  }, restOfProps);

  const inputAttributes = Object.assign({}, {
    'data-component-id': 'stationSearch'
  }, attributes);

  return createElement(document, ['input', inputProps, inputAttributes]);
}
