import {assert} from 'chai';
import {spy} from 'sinon';

import {document} from '../../testUtils/dom';

import StationSearch from './index';

describe('components/StationSearch', () => {

  it('should create an input' , () => {
    const el = StationSearch({document});
    assert.equal(el.tagName, 'INPUT', 'should be input');
    assert.equal(el.getAttribute('placeholder'), 'Station name or code', 'should add attributes');
  });

});