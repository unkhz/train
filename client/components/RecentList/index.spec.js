import {assert} from 'chai';
import {spy} from 'sinon';

import {document} from '../../testUtils/dom';

import RecentList from './index';

describe('components/RecentList', () => {

  it('should create an empty list without' , () => {
    const el = RecentList({document});
    assert.equal(el.tagName, 'DIV', 'should be div');
    assert.equal(el.querySelectorAll('li').length, 0, 'should be empty');
  });

  it('should create a filled list with data' , () => {
    const el = RecentList({document}, {data: ['A', 'B', 'C']});
    assert.equal(el.querySelectorAll('li').length, 3, 'should not be empty');
    assert.equal(el.querySelector('ul').textContent, 'ABC', 'should display textual data');
  });

});