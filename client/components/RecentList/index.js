import {createElement} from '../../modules/dom';

export default function RecentList({document, callAction}, props={}, attributes={}) {

  const {data=[], ...restOfProps} = props;

  const rootAttributes = Object.assign({}, {
    'data-component-id': 'recentList'
  }, attributes);


  let ul = ['ul', {}, {}, [
    // Data rows
    ...data.map((item, i) => {
      const onClick = (evt) => callAction('selectRecent', item);
      return ['li', {}, {}, [
        createElement(document, ['a', {onClick, href:'#'}, {}, [item]])
      ]];
    })

  ]];

  return createElement(document, ['div', restOfProps, rootAttributes, [
    ['h2', {}, {}, ['Recent views']],
    ul,
  ]]);
}