/**
 * Helper methods for unit tests
 */

import {jsdom} from 'jsdom';

export const document = jsdom("<html><body></body></html>", {
    features: {
    }
});

export const createFakeEvent = (eventType, bubbles=false, cancellable=false, eventClass='Event') => {
    const evt = document.createEvent(eventClass);
    evt.initEvent(eventType, bubbles, cancellable);
    return evt;
}

export const componentSelector = (componentId) => `[data-component-id='${componentId}']`;