import {assert} from 'chai';
import {spy} from 'sinon';

import {setState, onState, callAction, onAction} from './index';


describe('modules/state', () => {

  describe('setState', () => {

    it('should return modified state object', () => {
      assert.deepEqual(setState({}, 'foo', 'bar'), {foo: 'bar'});
    });

    it('should return modified state object with original contents', () => {
      assert.deepEqual(setState({test: 'a'}, 'foo', 'bar'), {test: 'a', foo: 'bar'});
    });

    it('should not modify state original object', () => {
      const state = {test: 'a'};
      setState('foo', 'bar');
      assert.deepEqual(state, {test: 'a'});
    });

    it('should call change event listener method on new property', () => {
      let state = {};
      const renderer = spy();
      state = onState(state, 'test', renderer);
      state = setState(state, 'test', 1);
      assert.deepEqual(renderer.firstCall.args[0], {test: 1});
    });

    it('should call change event listener method on changed property', () => {
      let state = setState({}, 'test', 0);
      const renderer = spy();
      state = onState(state, 'test', renderer);
      state = setState(state, 'test', 1);
      assert.deepEqual(renderer.firstCall.args[0], {test: 1});
    });

    it('should not call change event listener method when property value does not change', () => {
      let state = setState({}, 'test', 0);
      const renderer = spy();
      state = onState(state, 'test', renderer);
      state = setState(state, 'test', 0);
      assert.isTrue(renderer.notCalled);
    });

  });

  describe('callAction', () => {

    it('should call reaction method with value', () => {
      let state = {};
      const reaction = spy();
      state = onAction(state, 'test', reaction);
      state = callAction(state, 'test', 1);
      assert.deepEqual(reaction.firstCall.args[0], 1);
    });

    it('should call reaction method multiple times even with same value', () => {
      let state = setState({}, 'test', 0);
      const reaction = spy();
      state = onAction(state, 'test', reaction);
      state = callAction(state, 'test', 1);
      state = callAction(state, 'test', 1);
      state = callAction(state, 'test', 1);
      assert.deepEqual(reaction.getCall(0).args[0], 1);
      assert.deepEqual(reaction.getCall(1).args[0], 1);
      assert.deepEqual(reaction.getCall(2).args[0], 1);
    });

  });

});
