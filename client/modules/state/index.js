/**
 * Pure, immutable, one-directional state propagation mechanism
 *
 *   * UI/API events fire actions via callAction
 *   * reactions registered with onAction, are triggered by actions
 *   * reactions modify state via setState
 *   * renderers registered with onState, are triggered by state modifications
 *   * renderers update DOM
 */

export const setState = (state, key, value) => {
  const previousValue = state[key];
  if (value !== previousValue) {
    const {_renderers, _reactions, ...pureState} = state;
    if (_renderers) {
      const renderersForKey = _renderers[key] || [];
      renderersForKey.forEach((renderer) => renderer({
        ...pureState,
        [key]: value,
      }));
    }
    return {
      ...state,
      [key]: value,
    };
  } else {
    return state;
  }
};

export const onState = (state, key, renderer) => {
  const allRenderers = state._renderers || [];
  allRenderers[key] = allRenderers[key] || [];
  allRenderers[key].push(renderer);
  return {
    ...state,
    _renderers: allRenderers
  };
};

export const callAction = (state, key, value) => {
  const {_renderers, _reactions={}} = state;
  _reactions[key] = _reactions[key] || [];
  _reactions[key].forEach((reaction) => reaction(value));
  return {
    ...state,
    _reactions
  };
};

export const onAction = (state, key, reaction) => {
  const {_renderers, _reactions={}, ...pureState} = state;
  _reactions[key] = _reactions[key] || [];
  _reactions[key].push(reaction);
  return {
    ...state,
    _reactions
  };
};