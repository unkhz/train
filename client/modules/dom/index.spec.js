import {assert} from 'chai';
import {spy} from 'sinon';

import {document, createFakeEvent} from '../../testUtils/dom';

import {createElement} from './index';

describe('modules/dom', () => {

  describe('createElement', () => {

    it('should create a simple element' , () => {
      const el = createElement(document,
        ['a', {id: 'test-id', className: 'test-class'}, {href: 'test/url', 'data-test': 'data test'}]
      );
      assert.equal(el.tagName, 'A', 'should add correct tag');
      assert.equal(el.id, 'test-id', 'should add prop id');
      assert.equal(el.className, 'test-class', 'should add prop className');
      assert.deepEqual(el.getAttribute('href'), 'test/url', 'should add attribute href');
      assert.deepEqual(el.getAttribute('data-test'), 'data test', 'should add attribute data-test');
    });

    it('should create a simple element with a single text child node' , () => {
      const el = createElement(document,
        ['strong', {id: 'test-id'}, {}, ['test content']]
      );
      assert.equal(el.id, 'test-id', 'should add prop id');
      assert.equal(el.textContent, 'test content', 'should add text node');
    });

    it('should create a simple element with multiple text child nodes' , () => {
      const el = createElement(document,
        ['strong', {id: 'test-id'}, {}, ['test content', ' ', 'and some more']]
      );
      assert.equal(el.id, 'test-id', 'should add prop id');
      assert.equal(el.childNodes.length, 3, 'should add 3 nodes');
      assert.equal(el.children.length, 0, 'should add 0 Element nodes');
      assert.equal(el.textContent, 'test content and some more', 'should add content of both text nodes');
    });

    it('should allow Element instances to be specified as children' , () => {
      const el = createElement(document, ['ul', {}, {}, [
        createElement(document, ['li', {id:'li-1'}, {}, []]),
        ['li', {id:'li-2'}, {}, []],
      ]]);
      assert.equal(el.children.length, 2, 'should add 2 Element nodes');
      assert.equal(el.children[0].id, 'li-1', 'should add instance element');
      assert.equal(el.children[1].id, 'li-2', 'should add plain array element');
    });

    it('should create a simple element with an inline function event listener' , () => {
      const spies = {input: spy(), change: spy()};
      const el = createElement(document,
        ['input', {
          id: 'test-id',
          oninput: spies.input
        }, {
          onchange: spies.change
        }]
      );
      el.dispatchEvent(createFakeEvent('input'));
      el.dispatchEvent(createFakeEvent('change'));
      assert.equal(el.id, 'test-id', 'should add prop id');
      assert.isTrue(spies.input.calledOnce, 'should add event listener from props');
      assert.isTrue(spies.change.calledOnce, 'should add event listener from attributes');
    });

    it('should create an element with nested child elements' , () => {
      const el = createElement(document,
        ['ul', {id: 'test-ul-id'}, {}, [
          ['li', {id: 'li-1'}, {'data-test':0}, [
            ['a', {id: 'a-1', href:'url/1'}]
          ]],
          ['li', {id: 'li-2'}, {'data-test':1}, [
            ['a', {id: 'a-2', href:'url/2'}]
          ]],
          ['li', {id: 'li-3'}, {'data-test':2}, [
            ['a', {id: 'a-3', href:'url/3'}]
          ]]
        ]]
      );
      [0,1,2].forEach((i) => {
        var strId = '' + (i + 1);
        assert.equal(el.children[i].id, 'li-' + strId);
        assert.equal(el.children[i].getAttribute('data-test'), ''+i);
        assert.equal(el.children[i].children[0].id, 'a-' + strId);
        assert.equal(el.children[i].children[0].href, 'url/' + strId);
      });
    });

  });

});