/**
 * Encapsulates DOM operations
 */

/**
 * Create nested DOM Elements
 *
 * @param {Document} document The Document to use for element creation
 * @param {array} definition The definition tuple of the element to be created
 * @param {string} definition.nodeName The tag name of the Element
 * @param {object<string, *>} definition.props Key-value map of direct properties to be added to the element, E.g. classList
 * @param {object<string, string>} definition.attributes Key-value map of string attributes to be added to the element via setAttribute
 * @returns {Element}
 */
export function createElement(document, [nodeName, props={}, attributes={}, children=[]]) {
  var el = document.createElement(nodeName);

  Object.keys(props).forEach((key) => {
    const value = props[key];
    if (isFunctionEventListener(key, value)) {
      el.addEventListener(getEventName(key), value);
    } else {
      el[key] = props[key];
    }
  });

  Object.keys(attributes).forEach((key) => {
    const value = attributes[key];
    if (isFunctionEventListener(key, value)) {
      el.addEventListener(getEventName(key), value);
    } else {
      el.setAttribute(key, value);
    }
  });

  children.forEach((child) => {
    if (typeof child.appendChild === 'function') {
      el.appendChild(child);
    } else if (typeof child === 'string') {
      el.appendChild(document.createTextNode(child));
    } else {
      el.appendChild(createElement(document, child));
    }
  });
  return el;
}

function isFunctionEventListener(propName, value) {
  return propName.substring(0,2) === 'on' && typeof value === 'function';
}

function getEventName(propName) {
  return propName.substring(2).toLowerCase();
}

export const componentSelector = (componentId) => `[data-component-id='${componentId}']`;