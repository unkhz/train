import {assert} from 'chai';
import {spy} from 'sinon';

import createRequest from './huxley';


describe('modules/huxley', () => {

  describe('createRequest', () => {

    let httpRequest;
    beforeEach(() => {
      httpRequest = spy();
    });

    it('should call specified http request method with correct url', () => {
      const request = createRequest(httpRequest);
      request('ftp://localhost:8080', 'test-api-key', 'board', 'query');
      assert.deepEqual(httpRequest.getCall(0).args, ['ftp://localhost:8080/board/query/?accessToken=test-api-key']);
    });

    it('should call specified http request method with correct url if parts contain ending slash', () => {
      const request = createRequest(httpRequest);
      request('ftp://localhost:8080/', 'test-api-key', 'board/', 'query/');
      assert.deepEqual(httpRequest.getCall(0).args, ['ftp://localhost:8080/board/query/?accessToken=test-api-key']);
    });

    it('should call specified http request method with correct url if parts contain html entities', () => {
      const request = createRequest(httpRequest);
      request('ftp://localhost:8080/', 'test-api-key', 'board/', 'q,u.e&r?y/');
      assert.deepEqual(httpRequest.getCall(0).args, ['ftp://localhost:8080/board/q,u.ery/?accessToken=test-api-key']);
    });

    const args = [null, 'ftp://localhost:8080/', 'test-api-key', 'board/', 'query/'];
    for(let i=0; i<args.length; i++ ) {
      it('should call specified http request method with correct url when ' + i + ' arguments are specified in the factory method', () => {
        args[0] = httpRequest;
        const request = createRequest(...args.slice(0,i + 1));
        request(...args.slice(i + 1));
        assert.deepEqual(httpRequest.getCall(0).args, ['ftp://localhost:8080/board/query/?accessToken=test-api-key']);
      });
    }

  });

});