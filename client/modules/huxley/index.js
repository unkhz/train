/**
 * Encapsulates communication with Huxley JSON REST proxy for the UK National Rail Live Departure Board SOAP API
 */
import createRequest from './huxley';
export default createRequest;