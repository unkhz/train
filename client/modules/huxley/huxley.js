/**
 * Create a function for calling the Huxley JSON REST proxy for the UK National Rail Live Departure Board SOAP API
 *
 * @param {Function} httpRequestMethod
 * @param {string} [endPoint]
 * @param {string} [apiKey]
 * @param {string} [board]
 * @param {string} [query]
 * @returns {Function} A function with the speficied arguments bound
 */
export default function createRequest(...args) {
  return request.bind(null, ...args);
}

function request(httpRequestMethod, endPoint, apiKey, board, query) {
  return httpRequestMethod(createUrl(endPoint, apiKey, board, query));
}

function createUrl(endPoint, apiKey, board, query='') {
  const url = [endPoint, board, query].reduce((url, part) => (
    url + part.replace(/\/$/ , '').replace(/[?&]/g, '') + '/'
  ), '');
  return url + '?accessToken=' + apiKey;
}